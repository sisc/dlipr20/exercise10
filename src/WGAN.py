from comet_ml import Experiment
import dlipr
import numpy as np
from tensorflow import keras
from keras import layers
from keras.models import Sequential, Model
from keras.optimizers import Adam
from keras.utils import plot_model
from functools import partial
import keras.backend.tensorflow_backend as KTF
import utils
import matplotlib.pyplot as plt


# Set up YOUR experiment - login to comet, create new project (for new exercise)
# and copy the statet command
# or just change the name of the workspace, and the API (you can find it in the settings)
experiment = Experiment(api_key="<HIDDEN>",
                        project_name="exercise10", workspace="irratzo")

log_dir = "."

# basic training parameter do not change them
EPOCHS = 20
GRADIENT_PENALTY_WEIGHT = 10
BATCH_SIZE = 128
NCR = 5
latent_size = 64
learning_rate = 0.0001

# load training data
shower_maps, Energy = utils.ReadInData()
print("Shape of shower maps", shower_maps.shape)
N = shower_maps.shape[0]
# plot real signal patterns
utils.plot_multiple_signalmaps(shower_maps[:, :, :, 0], log_dir=log_dir, title='Footprints', epoch='Real')



# Task 1: Build a generator and a critic network which allows for the generation of 9x9 air-shower footprints
# and hand in the model showing the tensor shapes. (Hint A: use keras.utils.plot_model with show_shapes=True)
# Hint B: make use of the DCGAN guidelines and use ReLU as last activation in the generator to set a sparsity prior.


def build_generator(latent_size):
    n_channels=1
    generator = Sequential([
        layers.Dense(9*9*n_channels,activity_regularizer=keras.regularizers.l1_l2(1e-5)),
        layers.BatchNormalization(),
        layers.Activation('relu'),
        layers.Reshape([9,9,1]),
        #layers.UpSampling2D(size=(2,2)),
        layers.Conv2D(1, (3, 3), padding='same'),
        #layers.BatchNormalization(),
        #layers.Activation('relu'),
        #layers.Conv2D(n_channels // 4, (3, 3), padding='same'),
        #layers.BatchNormalization(),
        #layers.Activation('relu'),
        layers.Conv2D(1, (1, 1), padding='same', activation='sigmoid')
        ],
        name='generator')
    #generator.compile(loss='binary_crossentropy', optimizer=keras.optimizers.Adam(lr=1e-4))
    return generator

def build_critic():
    drop_rate=0.25
    critic = Sequential([
        layers.Conv2D(256, (3, 3), input_shape=[9,9,1], padding='same', activation='relu'),
        layers.LeakyReLU(0.2),
        layers.Dropout(drop_rate),
        layers.Conv2D(512, (3, 3), padding='same', activation='relu'),
        layers.LeakyReLU(0.2),
        layers.Dropout(drop_rate),
        layers.Flatten(),
        layers.Dense(256, activity_regularizer=keras.regularizers.l1_l2(1e-5)),
        layers.LeakyReLU(0.2),
        layers.Dropout(drop_rate),
        layers.Dense(2, activation='softmax')
        ],
        name='critic')
    #generator.compile(loss='binary_crossentropy', optimizer=keras.optimizers.Adam(lr=1e-4))
    return critic

generator = build_generator(latent_size)
#print(generator.summary())
critic = build_critic()
#print(critic.summary())



# Taks 2: Set up the training by implementing the Wasserstein loss using the Kantorovich-Rubinstein duality.
# The gradient penalty is already there.
# Just implement the substraction c(x) - c(G(z)) which you then have to maximize/minimize.


def wasserstein_loss(y_true, y_pred):
    """Use the Kantorovich-Rubinstein duality to approximate the Wasserstein distance.
    Hint: The gradient penalty is already implemented.
    You just have to construct the substraction by making use of the labels.
    You HAVE to make use of the KTF functions."""
    y = KTF.max(y_true-y_pred)
    #print(y.labels()
    print(y)
    #print(y["loss/critic_loss/wasserstein_loss/Max"])
    return y
    
# Make use of the labels to define the Wasserstein loss
dummy = np.zeros(BATCH_SIZE)  # keras throws an error when calculating a loss without having a label -> needed for using the gradient penalty loss


# make generator_training graph for generator step (set generator on top of critic)
utils.make_trainable(critic, False)  # freeze the critic during the generator training
generator_training = utils.build_generator_graph(generator, critic, latent_size)
generator_training.compile(optimizer=Adam(learning_rate, beta_1=0.5, beta_2=0.9), loss=[wasserstein_loss])

# make trainings graph for critic step  (set generator on top of critic and add gradient penalty part.)
# critic_training graph get as inputs noise (for generator) and real shower maps (for measure Wasserstein distance between generated and original shower maps)

utils.make_trainable(generator, False)  # freeze the generator during the critic training
utils.make_trainable(critic, True)  # unfreeze the critic during the critic training

critic_training, averaged_batch = utils.build_critic_graph(generator, critic, latent_size, batch_size=BATCH_SIZE)
gradient_penalty = partial(utils.gradient_penalty_loss, averaged_batch=averaged_batch, penalty_weight=GRADIENT_PENALTY_WEIGHT)  # construct the gradient penalty
gradient_penalty.__name__ = 'gradient_penalty'
plot_model(critic_training, to_file=log_dir + '/critic_training.png', show_shapes=True)

critic_training.compile(optimizer=Adam(learning_rate, beta_1=0.5, beta_2=0.9), loss=[wasserstein_loss, wasserstein_loss, gradient_penalty])


generator_loss = []
critic_loss = []


# Task 2: Pretrain the critic for one epoch. Therefore, feed noise and real shower images into the critic training graph
# which directly connects the generator witch the critic. So that you get rid of the generator prediction phase.
# (For details, have a look on the critic_training.png plot, which visualizes the critic_training graph)
# Hand in the plot of the converging critic loss.
# (If the critic loss (approximation of the Wasserstein distance) do not converge, your implementation may wrong, try to remove BatchNormalization layers!)





#critic_training.fit([np.empty((100000,64)),shower_maps], epochs=1, batch_size=BATCH_SIZE)

#critic_training.fit([np.empty((100000,64)),shower_maps],[np.zeros(100000),np.ones(100000),averaged_batch], epochs=1, batch_size=BATCH_SIZE)

# Task 3: Implement the main loop
# - Train the critic for NCR iterations (using noise and real samples)
# - Than train the generator for 1 iteration
# - Repeat the procedure for the entire dataset (1 epoch)
# - Train the framework for 10 epochs and hand in the critic loss and the generated air shower samples.
# ! Don't forget to track your figures using "experiment.log_figure(figure=plt)" !

