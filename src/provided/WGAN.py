from comet_ml import Experiment
import dlipr
import numpy as np
from tensorflow import keras
from keras.models import Sequential, Model
from keras.optimizers import Adam
from keras.utils import plot_model
from functools import partial
import keras.backend.tensorflow_backend as KTF
import utils


# Set up YOUR experiment - login to comet, create new project (for new exercise)
# and copy the statet command
# or just change the name of the workspace, and the API (you can find it in the settings)
experiment = Experiment(api_key="EnterYourAPIKey",
                        project_name="exercise10", workspace="EnterGroupWorkspaceHere")

log_dir = "."

# basic training parameter do not change them
EPOCHS = 20
GRADIENT_PENALTY_WEIGHT = 10
BATCH_SIZE = 128
NCR = 5
latent_size = 64
learning_rate = 0.0001

# load training data
shower_maps, Energy = utils.ReadInData()
print("Shape of shower maps", shower_maps.shape)
N = shower_maps.shape[0]
# plot real signal patterns
utils.plot_multiple_signalmaps(shower_maps[:, :, :, 0], log_dir=log_dir, title='Footprints', epoch='Real')



# Task 1: Build a generator and a critic network which allows for the generation of 9x9 air-shower footprints
# and hand in the model showing the tensor shapes. (Hint A: use keras.utils.plot_model with show_shapes=True)
# Hint B: make use of the DCGAN guidelines and use ReLU as last activation in the generator to set a sparsity prior.


def build_generator(latent_size):
    generator = Sequential(name='generator')
    return generator

def build_critic():
    critic = Sequential(name='critic')
    return critic

generator = build_generator(latent_size)
print(generator.summary())
critic = build_critic()
print(critic.summary())



# Taks 2: Set up the training by implementing the Wasserstein loss using the Kantorovich-Rubinstein duality.
# The gradient penalty is already there.
# Just implement the substraction c(x) - c(G(z)) which you then have to maximize/minimize.


def wasserstein_loss(y_true, y_pred):
    """Use the Kantorovich-Rubinstein duality to approximate the Wasserstein distance.
    Hint: The gradient penalty is already implemented.
    You just have to construct the substraction by making use of the labels.
    You HAVE to make use of the KTF functions."""
    return 1
    
# Make use of the labels to define the Wasserstein loss
dummy = np.zeros(BATCH_SIZE)  # keras throws an error when calculating a loss without having a label -> needed for using the gradient penalty loss


# make generator_training graph for generator step (set generator on top of critic)
utils.make_trainable(critic, False)  # freeze the critic during the generator training
generator_training = utils.build_generator_graph(generator, critic, latent_size)
generator_training.compile(optimizer=Adam(learning_rate, beta_1=0.5, beta_2=0.9), loss=[wasserstein_loss])

# make trainings graph for critic step  (set generator on top of critic and add gradient penalty part.)
# critic_training graph get as inputs noise (for generator) and real shower maps (for measure Wasserstein distance between generated and original shower maps)

utils.make_trainable(generator, False)  # freeze the generator during the critic training
utils.make_trainable(critic, True)  # unfreeze the critic during the critic training

critic_training, averaged_batch = utils.build_critic_graph(generator, critic, latent_size, batch_size=BATCH_SIZE)
gradient_penalty = partial(utils.gradient_penalty_loss, averaged_batch=averaged_batch, penalty_weight=GRADIENT_PENALTY_WEIGHT)  # construct the gradient penalty
gradient_penalty.__name__ = 'gradient_penalty'
plot_model(critic_training, to_file=log_dir + '/critic_training.png', show_shapes=True)

critic_training.compile(optimizer=Adam(learning_rate, beta_1=0.5, beta_2=0.9), loss=[wasserstein_loss, wasserstein_loss, gradient_penalty])


generator_loss = []
critic_loss = []


# Task 2: Pretrain the critic for one epoch. Therefore, feed noise and real shower images into the critic training graph
# which directly connects the generator witch the critic. So that you get rid of the generator prediction phase.
# (For details, have a look on the critic_training.png plot, which visualizes the critic_training graph)
# Hand in the plot of the converging critic loss.
# (If the critic loss (approximation of the Wasserstein distance) do not converge, your implementation may wrong, try to remove BatchNormalization layers!)


# Task 3: Implement the main loop
# - Train the critic for NCR iterations (using noise and real samples)
# - Than train the generator for 1 iteration
# - Repeat the procedure for the entire dataset (1 epoch)
# - Train the framework for 10 epochs and hand in the critic loss and the generated air shower samples.
# ! Don't forget to track your figures using "experiment.log_figure(figure=plt)" !

