# Correction
## Task 1 
###Task 1.1
4/6 P - you have 2 dim critic output. You can do that, but then Wasserstein metric needs to be defined differently. Better to keep it 1 dim. Additionally you used sigmoid activation on the generator, this cannot lead to comparable generated results, since sigmoid -&gt; (0,1) / also quite small networks
###Task 1.2
1/5 P - no loss curve. It did not work, since you defined your critic output to be 2 dim, but you are feeding 1 dim results (np.ones(100000) is 100000 x 1 dim and not 100000 x 2 dim ), also Wasserstein metric not correct. It says only about the worst placed datapoint and not about the distribution. 1P for data preparation
###Task 1.3
0/5 P - did not work
## Task 2
###Task 2.1
2/2 P
###Task 2.2
3/3 P
#Total : 10/21 P



**RWTH Aachen - Deep Learning in Physics Research SS2020**

**Exercise 10 **

Date: 06.07.2020

Students:
| student            | rwth ID |
|--------------------|---------|
| Baptiste Corneglio | 411835|
| Florian Stadtmann  | 367319 |
| Johannes Wasmer    |  090800 |



# Task 1: Simulate air shower footprints using a Wasserstein GAN #

## I. Build a generator and a critic network which allows for the generation of 9x9 air shower footprints. ##

For building the generator and discriminator, we start with the generative and discriminative networks from exercise 9 and change them in a way tobeing able to deal with the (9,9,1) input data instead of (28,28,n_channels). The intention was to later adjust them, but an unresolved error during pretraining of the discriminator made it impossible.

    generator = Sequential([
            layers.Dense(9*9,activity_regularizer=keras.regularizers.l1_l2(1e-5)),
            layers.BatchNormalization(),
            layers.Activation('relu'),
            layers.Reshape([9,9,1]),
            layers.Conv2D(1, (3, 3), padding='same')
            layers.Conv2D(1, (1, 1), padding='same', activation='sigmoid')
            ],
            name='generator')

    critic = Sequential([
            layers.Conv2D(256, (3, 3), input_shape=[9,9,1], padding='same', activation='relu'),
            layers.LeakyReLU(0.2),
            layers.Dropout(drop_rate),
            layers.Conv2D(512, (3, 3), padding='same', activation='relu'),
            layers.LeakyReLU(0.2),
            layers.Dropout(drop_rate),
            layers.Flatten(),
            layers.Dense(256, activity_regularizer=keras.regularizers.l1_l2(1e-5)),
            layers.LeakyReLU(0.2),
            layers.Dropout(drop_rate),
            layers.Dense(2, activation='softmax')
            ],
            name='critic')


## II. Set up the training by implementing the Wasserstein loss using the KantorovichRubinstein duality. Pretrain the critic for one epoch and hand in the loss curve. ##

We implemented the waterstein distance by inserting 

    y = KTF.max(y_true-y_pred)

to calculate the supremum of the difference between the true and predicted label.

Until now, the program code is executable without any error messages. Unfortunately, this changes wehn we tried to train the network.

For training the network, the following line was used by figuring out what to insert with a look in the utils python file.

    critic_training.fit([np.empty((100000,64)),shower_maps], epochs=1, batch_size=BATCH_SIZE)

but during the training, keras gives and error 

    Traceback (most recent call last):
    File &quot;WGAN.py&quot;, line 146, in 
        critic_training.fit([np.empty((100000,64)),shower_maps], epochs=1, batch_size=BATCH_SIZE)
    File &quot;/software/python3.5/lib/python3.5/site-packages/comet_ml/monkey_patching.py&quot;, line 292, in wrapper
        return_value = original(*args, **kwargs)
    File &quot;/software/python3.5/lib/python3.5/site-packages/keras/engine/training.py&quot;, line 1239, in fit
        validation_freq=validation_freq)
    File &quot;/software/python3.5/lib/python3.5/site-packages/keras/engine/training_arrays.py&quot;, line 141, in fit_loop
        if issparse(fit_inputs[i]) and not K.is_sparse(feed[i]):
    IndexError: list index out of range

It indicates that while iterating over the inputs, an index is out of range. There are no correct predictions inserted yet, so we tried the same with giving the y results:

    critic_training.fit([np.empty((100000,64)),shower_maps],[np.zeros(100000),np.ones(100000),averaged_batch], epochs=1, batch_size=BATCH_SIZE)

This time we got another error fuction:

    Traceback (most recent call last):
    File &quot;WGAN.py&quot;, line 146, in 
        critic_training.fit([np.empty((100000,64)),shower_maps],[np.zeros(100000),np.ones(100000),averaged_batch], epochs=1, batch_size=BATCH_SIZE)
    File &quot;/software/python3.5/lib/python3.5/site-packages/comet_ml/monkey_patching.py&quot;, line 292, in wrapper
        return_value = original(*args, **kwargs)
    File &quot;/software/python3.5/lib/python3.5/site-packages/keras/engine/training.py&quot;, line 1154, in fit
        batch_size=batch_size)
    File &quot;/software/python3.5/lib/python3.5/site-packages/keras/engine/training.py&quot;, line 621, in _standardize_user_data
        exception_prefix='target')
    File &quot;/software/python3.5/lib/python3.5/site-packages/keras/engine/training_utils.py&quot;, line 99, in standardize_input_data
        data = [standardize_single_array(x) for x in data]
    File &quot;/software/python3.5/lib/python3.5/site-packages/keras/engine/training_utils.py&quot;, line 99, in 
        data = [standardize_single_array(x) for x in data]
    File &quot;/software/python3.5/lib/python3.5/site-packages/keras/engine/training_utils.py&quot;, line 32, in standardize_single_array
        'Got tensor with shape: %s' % str(shape))
    ValueError: When feeding symbolic tensors to a model, we expect the tensors to have a static batch size. Got tensor with shape: (None, 9, 9, 1)

In the end, we were not able to figure out how the training information have to be inserted into the critic_training network that uses the generator and RandomWeightedAverage in front of the actual critic.  

## III. Implement the main loop and train the framework for 15 epochs. Hand in the plot of the critic loss and generated air shower footprints. ###

Because of this, we were not able to do Task1.III but instead continued with Task 2

# Task 2:  Wasserstein GAN #

## I. Name 4 general challenges of training adversarial frameworks ##

1. Two networks have to be trained both. To achive a good training, one alternates between the generative and discriminative network. But there it is difficult to keep both networks competitive. If one network pulls ahead, the other one might not pull up again. The Nash equilibrium has to be found.

2. The training of both networks are correlated. If the discriminator goes astray, the generator will not give meaningful results any more.

3. The loss can be hard to interpret, depending on the discriminator.

4. If the generator begins focusing on certain classes that are easy to fool the discriminator with, instead of all classes while training, the generator will later not be able to reliable produce other classes. This is called &quot;mode collapsing&quot;. 

## II. Explain why approximating the Wasserstein distance in the discriminator/critic helps to reduce mode collapsing  ##

Approximating the Waterstein distance in the discriminator reduces the mode collapsing because the softer metric does not focus on single points in the phasespace and therefore the discriminator does not force the generator to cycle between different classes. With the Lipschitz constrain, the convergence of the discriminator is ensured with a meaningful and stable gradient.
